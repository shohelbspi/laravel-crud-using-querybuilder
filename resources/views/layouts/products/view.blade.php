@extends('layouts/master')

@section('title')
Category View Page
    
@endsection


@section('body')
<div class="container my-4">
    <div class="card" style="width: 50%">
        <div class="card-header">
            <a href="{{route ('product.index')}}"><button style="float: right" class="btn btn-success">All Products </button></a>
            Product Data
        </div>
        <div class="card-body">
            <p><h4>Name:</h4> {{$viewData->name}}</p>
            <p><h4>Title:</h4> {{$viewData->title}}</p>
            <p><h4>Price:</h4> {{$viewData->price}}</p>
            <p><h4>Description:</h4> {{$viewData->description}}</p>
        </div>
    </div>
</div>
    
@endsection