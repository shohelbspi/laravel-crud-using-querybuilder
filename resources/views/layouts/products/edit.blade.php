@extends('layouts/master')

@section('title')
Product Edit Page
    
@endsection


@section('body')
<div class="container my-4">
    <div class="card">
        <div class="card-header">
            <a href="{{route ('category.index')}}"><button style="float: right" class="btn btn-success">All Products</button></a>
            <h2>Edit Producr</h2>
        </div>
        <div class="card-body">
            <form action="{{url ('update-product/'.$editData->id)}}" method="POST">
              @csrf
                <div class="mb-3">
                  <label for="name" class="form-label">Name</label>
                  <input type="text" class="form-control" value="{{$editData->name}}" name="name" id="name" aria-describedby="name">
                </div>

                <select class="form-select" aria-label="Default select example" name="cat_id">
                  @foreach ($data as $item)
        
                  @if($item->id == $editData->cat_id) 
                    <option value="{{$item->id}}" selected>{{$item->name}}</option>
                  
                  @else

                    <option value="{{$item->id}}">{{$item->name}}</option>
                  @endif
                  
                  @endforeach
                </select>

                <div class="mb-3">
                  <label for="title" class="form-label">Title</label>
                  <input type="text" class="form-control" value="{{$editData->title}}" name="title" id="title" aria-describedby="title">
                </div>

                <div class="mb-3">
                    <label for="price" class="form-label">Price</label>
                    <input type="text" class="form-control" value="{{$editData->price}}" name="price" id="price" aria-describedby="price">
                  </div>

                  <div class="mb-3">
                    <label for="description" class="form-label">Description</label>
                    <input type="text" class="form-control" value="{{$editData->description}}" name="description" id="description" aria-describedby="description">
                  </div>


               
                <button type="submit" class="btn btn-primary">Update</button>
              </form>
        </div>
    </div>
</div>
    
    
@endsection