@extends('layouts/master')

@section('title')
Product index Page

@endsection


@section('body')
<div class="container my-4">
    <div class="card">
        <div class="card-header">
            <a href="{{route ('product.create')}}"><button style="float: right" class="btn btn-success">Add
                    Product</button></a>
            <h2>All Product</h2>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">SL</th>
                        <th scope="col">Name</th>
                        <th scope="col">title</th>
                        <th scope="col">price</th>
                        <th scope="col">description</th>
                        <th scope="col">Category Id</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>



                <tbody>

                    @foreach ($product as $item)
                    {{-- @php
                        $sl = 0
                    @endphp
                    <tr> --}}
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->title}}</td>
                        <td>{{$item->price}}</td>
                        <td>{{$item->description}}</td>
                        <td>{{$item->cat_name}}</td>
                        <td class='mx-2'>
                            <a href="{{url ('view-product/'.$item->id)}}"><button
                                    class="btn btn-success">View</button></a>
                            <a href="{{url ('edit-product/'.$item->id)}}"><button
                                    class="btn btn-primary">Edit</button></a>
                            <a href="{{url ('delete-product/'.$item->id)}}"><button
                                    class="btn btn-danger">Delete</button></a>
                        </td>
                    </tr>

                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection