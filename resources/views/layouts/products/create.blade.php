@extends('layouts/master')

@section('title')
Product Create Page

@endsection


@section('body')
<div class="container my-4">
  <div class="card">
    <div class="card-header">
      <a href="{{route ('product.index')}}"><button style="float: right" class="btn btn-success">All
          Product</button></a>
      <h2>Add Product</h2>
    </div>
    <div class="card-body">
      <form action="{{route('product.store')}}" method="POST">
        @csrf
        <div class="mb-3">
          <label for="name" class="form-label">Name</label>
          <input type="text" class="form-control" name="name" id="name" aria-describedby="name">
        </div>

        <select class="form-select" aria-label="Default select example" name="cat_id">
          @foreach ($data as $item)

          <option value="{{$item->id}}">{{$item->name}}</option>

          @endforeach
        </select>

        <div class="mb-3">
          <label for="title" class="form-label">Title</label>
          <input type="text" class="form-control" name="title" id="title" aria-describedby="tile">
        </div>

        <div class="mb-3">
          <label for="price" class="form-label">price</label>
          <input type="text" class="form-control" name="price" id="price" aria-describedby="price">
        </div>

        <div class="mb-3">
          <label for="description" class="form-label">Description</label>
          <input type="text" class="form-control" name="description" id="description" aria-describedby="description">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </div>
</div>

@endsection