@extends('layouts/master')

@section('title')
Category Edit Page
    
@endsection


@section('body')
<div class="container my-4">
    <div class="card">
        <div class="card-header">
            <a href="{{route ('category.index')}}"><button style="float: right" class="btn btn-success">All Category</button></a>
            <h2>Edit Category</h2>
        </div>
        <div class="card-body">
            <form action="{{url ('update-category/'.$editData->id)}}" method="POST">
              @csrf
                <div class="mb-3">
                  <label for="name" class="form-label">Name</label>
                  <input type="text" class="form-control" value="{{$editData->name}}" name="name" id="name" aria-describedby="name">
                </div>

                <div class="mb-3">
                  <label for="slug" class="form-label">Slug</label>
                  <input type="text" class="form-control" value="{{$editData->slug}}" name="slug" id="slug" aria-describedby="slug">
                </div>
               
                <button type="submit" class="btn btn-primary">Update</button>
              </form>
        </div>
    </div>
</div>
    
    
@endsection