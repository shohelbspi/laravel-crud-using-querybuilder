@extends('layouts/master')

@section('title')
Category View Page
    
@endsection


@section('body')
<div class="container my-4">
    <div class="card" style="width: 30%">
        <div class="card-header">
            <a href="{{route ('category.index')}}"><button style="float: right" class="btn btn-success">All Category</button></a>
            Category Data
        </div>
        <div class="card-body">
            <p>Name: {{$viewData->name}}</p>
            <p>Slug: {{$viewData->slug}}</p>
        </div>
    </div>
</div>
    
@endsection