<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/// Category Route

Route::get('/index-category', [CategoryController::class, 'index'])->name('category.index');
Route::get('/create-category', [CategoryController::class, 'create'])->name('category.create');
Route::post('/store-category', [CategoryController::class, 'store'])->name('category.store');
Route::get('/view-category/{id}', [CategoryController::class, 'view'])->name('category.view');
Route::get('/edit-category/{id}', [CategoryController::class, 'edit'])->name('category.edit');
Route::post('/update-category/{id}', [CategoryController::class, 'update'])->name('category.update');
Route::get('/delete-category/{id}', [CategoryController::class, 'delete'])->name('category.delete');

//Product Route
Route::get('/index-product', [ProductsController::class, 'index'])->name('product.index');
Route::get('/create-product', [ProductsController::class, 'create'])->name('product.create');
Route::post('/store-product', [ProductsController::class, 'store'])->name('product.store');
Route::get('/view-product/{id}', [ProductsController::class, 'view'])->name('product.view');
Route::get('/edit-product/{id}', [ProductsController::class, 'edit'])->name('product.edit');
Route::post('/update-product/{id}', [ProductsController::class, 'update'])->name('product.update');
Route::get('/delete-product/{id}', [ProductsController::class, 'delete'])->name('product.delete');
