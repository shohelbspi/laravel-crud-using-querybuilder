<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProductsController extends Controller
{
    public function index()
    {
        // $product = DB::table('products')->get();
        $product = DB::table('products')
            ->join('categories', 'products.cat_id', '=', 'categories.id')
            // ->join('categories', 'products.cat_id', '=', 'categories.id')
            ->select('products.*','categories.name as cat_name' )
            ->get();
        return view('layouts/products/index', compact('product'));
    }

    public function create()
    {
        $data = DB::table('categories')->get();
        return view('layouts/products/create', compact('data'));
    }

    public function store(Request $request)
    {
        $data = array();

        $data['name'] = $request->name;
        $data['title'] = $request->title;
        $data['price'] = $request->price;
        $data['description'] = $request->description;
        $data['cat_id'] = $request->cat_id;

        DB::table('products')->insert($data);

        return redirect('index-product');
    }

    public function view($id)
    {
        $viewData = DB::table('products')->where('id', $id)->first();
        
        return view('layouts/products/view', compact('viewData'));
    }

    public function edit($id)
    {
        $editData = DB::table('products')->where('id', $id)->first();
        $data = DB::table('categories')->get(); 
        return view('layouts/products/edit', compact('editData','data'));
    }

    public function update(Request $request, $id)
    {
        $data = array();

        $data['name'] = $request->name;
        $data['title'] = $request->title;
        $data['price'] = $request->price;
        $data['description'] = $request->description;
        $data['cat_id'] = $request->cat_id;
        $data = DB::table('products')->where('id', $id)->update($data);
        return redirect('index-product');
    }

    public function delete($id)
    {
        $data = DB::table('products')->where('id',$id)->delete();
        return redirect('index-product');
    }
}
