<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CategoryController extends Controller
{
    public function index()
    {
        $category = DB::table('categories')->get();
        return view('layouts/category/index', compact('category'));
    }

    public function create()
    {
        return view('layouts/category/create');
    }

    public function store(Request $request)
    {
        $data = array();
        $data['name'] = $request->get('name');
        $data['slug'] = $request->get('slug');

        DB::table('categories')->insert($data);
        return redirect('index-category');
    }

    public function view($id)
    {
        $viewData = DB::table('categories')->where('id', $id)->first();
        return view('layouts/category/view', compact('viewData'));
    }

    public function edit($id)
    {
        $editData = DB::table('categories')->where('id', $id)->first();
        return view('layouts/category/edit', compact('editData'));
    }

    public function update(Request $request, $id)
    {
        $data = array();
        $data['name'] = $request->get('name');
        $data['slug'] = $request->get('slug');

        $data = DB::table('categories')->where('id', $id)->update($data);
        return redirect('index-category');
    }

    public function delete($id)
    {
        DB::table('categories')->where('id', $id)->delete();
        return redirect('index-category');
    }
}
